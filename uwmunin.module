<?php

/**
 * @file
 * Custom munin routines.
 */

/**
 * Implements hook_muninplugin().
 */
function uwmunin_muninplugin() {
  $plugins = array();
  $plugins['uwusers'] = array(
    '#title' => t('Users information'),
    '#description' => t('Information about the users on your site.'),
    '#vlabel' => t('Number'),
  );
  $plugins['uwusers']['allusers'] = array(
    '#label' => t('All users'),
    '#description' => t(
      'This item shows the total number of users that have been created.'
    ),
    '#statement' => 'SELECT COUNT(uid) FROM {users}',
  );
  $plugins['uwusers']['allinactiveusers'] = array(
    '#label' => t('All not active users'),
    '#description' => t(
      'This item shows the total number of inactive users that have been created.'
    ),
    '#statement' => 'SELECT COUNT(uid) FROM {users} WHERE uid not in (SELECT DISTINCT(uid) FROM {node})',
  );
  $plugins['uwsmart'] = array(
    '#title' => t('Smart statistics'),
    '#description' => t(
      'Information about existing and new content like nodes and comments.'
    ),
    '#vlabel' => t('Amount'),
  );
  $plugins['uwsmart']['nodesperuser'] = array(
    '#label' => t('Nodes/Users'),
    '#description' => t('Number of existing nodes/users on this site.'),
    '#statement' => 'SELECT (SELECT COUNT(nid) FROM {node}) / (SELECT COUNT(uid) FROM {users})',
  );
  $plugins['uwsmart2'] = array(
    '#title' => t('Smart statistics'),
    '#description' => t(
      'Information about existing and new content like nodes and comments.'
    ),
    '#vlabel' => t('Amount'),
  );
  $plugins['uwsmart2']['userspernodes'] = array(
    '#label' => t('Users/Nodes'),
    '#description' => t('Number of existing users/nodes on this site.'),
    '#statement' => 'SELECT (SELECT COUNT(uid) FROM {users}) / (SELECT COUNT(nid) FROM {node})',
  );
  if (module_exists('domain')) {
    $counts = db_query(
      "SELECT COUNT(n.nid) as c, d.subdomain FROM {node} n JOIN {domain_access} AS da ON (n.nid = da.nid AND da.realm = 'domain_id') JOIN {domain} AS d ON (da.gid = d.domain_id) GROUP BY d.subdomain ORDER BY c DESC"
    )->fetchAllKeyed();
    if (count($counts)) {
      $plugins['uwdomains'] = array(
        '#title' => t('Smart domains statistics'),
        '#description' => t('Information about existing content per domain.'),
        '#vlabel' => t('Amount'),
      );
      foreach ($counts as $count => $subdomain) {
        if ($count == 0) {
          continue;
        }
        $subdomain_name = str_replace('.', '', $subdomain);
        $subdomain_name = str_replace('-', '', $subdomain_name);
        $subdomain_name = str_replace('_', '', $subdomain_name);
        $plugins['uwdomains'][$subdomain_name] = array(
          '#label' => t('Domain nodes ' . $subdomain),
          '#description' => t('Number of existing nodes on the domain:' . $subdomain . ' count: ' . $count),
          '#callback' => 'uwmunin_count',
          '#callback_arguments' => array($count),
          '#statement' => '',
          '#sql' => '',
          '#file' => '',
        );
      }
    }
  }

  $plugins['uwhhvm'] = array(
    '#title' => t('Is HHVM php backend?'),
    '#description' => t('State of running on HHVM or not.'),
    '#vlabel' => t('Status'),
  );

  $cached = cache_get('hhvm_state');
  // @see http://stackoverflow.com/a/20777835/3027445
  $hhvm_state = $cached ? $cached->data : defined('HHVM_VERSION') ? 1 : 0;

  $plugins['uwhhvm']['status'] = array(
    '#label' => t('Is HHVM?'),
    '#decription' => t('State of running on HHVM or not.'),
    '#callback' => 'uwmunin_count',
    '#callback_arguments' => array($hhvm_state),
    '#statement' => '',
    '#sql' => '',
    '#file' => ''
  );

  return $plugins;
}

/**
 * Callback for domains.
 */
function uwmunin_count($num) {
  return $num;
}

/**
 * Implements hook_init().
 */
function uwmunin_init() {
  if (PHP_SAPI == 'cli' || function_exists('drush_main')) {
    // Get out when it is a console.
    return;
  }
  $hhvm_state = defined('HHVM_VERSION') ? 1 : 0;
  $cached = cache_get('hhvm_state');
  if (!$cached) {
    cache_set('hhvm_state', $hhvm_state);
    return;
  }

  if ($cached->data != $hhvm_state) {
    // State changed, let's update data.
    cache_set('hhvm_state', $hhvm_state);
  }
}
